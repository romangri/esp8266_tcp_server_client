/*
 * ssd1306.h
 *
 *  Created on: 12 mars 2017
 *      Author: Jens
 */

#ifndef INCLUDE_SSD1306_H_
#define INCLUDE_SSD1306_H_

#define ADDRESS			(0x3c<<1)
#define WRITE			0
#define READ			1
#define HORIZONTAL		0
#define VERTICAL		1
#define COMMAND			0x00
#define DATA			0x40
#define STARTBIT		i2c_master_start()	//SCK=HIGH
#define STOPBIT			i2c_master_stop()	//SCK=HIGH

#define CONTRAST		0x81	//A
#define DISPLAYON(x)	(0xa4+x)
#define INVERSE(x)		(0xa6+x)
#define ONOFF(x)		(0xae +x)

#define LOCOL(x)		(x)
#define HICOL(x)		(0x10+x)
#define ADDRESSING		0x20	//A
#define SETCOL			0x21	//AB
#define SETPAGE			0x22	//AB
#define SETPAGESTART(x)	(0xb0+x)

#define STARTLINE(x)	(0x40+x)
#define REMAP(x)		(0xa0+x)
#define MULTIPLEX		0xa8	//A
#define OUTDIR(x)		(0xc0+(x<<3))
#define OFFSET			0xd3	//A
#define COMPINS			0xda	//A

#define DIVIDEFREQ		0xd5	//A
#define PRECHARGE		0xd9	//A
#define VCOMH			0xdb	//A
#define NOP				0xe3

#define CHARGEPUMP		0x8d	//A

#define sendbyte(X)		(i2c_master_writeByte(X), i2c_master_checkAck())

//##
//#define sendNible(x)	(i2c_master_writeNible(x), i2c_master_checkAck())

#define DISABLESCROLL	0x2e

#define SSD1306_INIT(DIRECTION)\
	STOPBIT;\
	STARTBIT;\
	sendbyte(ADDRESS+WRITE);\
	sendbyte(COMMAND);\
	sendbyte(ONOFF(0));\
	sendbyte(DIVIDEFREQ);	sendbyte(0x80);\
	sendbyte(MULTIPLEX);	sendbyte(63);\
	sendbyte(OFFSET);		sendbyte(0);\
	sendbyte(STARTLINE(0));\
	sendbyte(CHARGEPUMP);	sendbyte(0x14);\
	sendbyte(ADDRESSING);	sendbyte(DIRECTION);\
	sendbyte(REMAP(1));\
	sendbyte(OUTDIR(1));\
	sendbyte(COMPINS);		sendbyte(0x12);\
	sendbyte(CONTRAST);		sendbyte(0xcf);\
	sendbyte(PRECHARGE);	sendbyte(0xf1);\
	sendbyte(VCOMH);		sendbyte(0x40);\
	sendbyte(DISPLAYON(0));\
	sendbyte(INVERSE(0));\
	sendbyte(DISABLESCROLL);\
	sendbyte(ONOFF(1));\
	sendbyte(SETCOL);		sendbyte(0);	sendbyte(127);\
	sendbyte(SETPAGE);		sendbyte(0);	sendbyte(7);\
	STOPBIT;\
	STARTBIT;\
	sendbyte(ADDRESS+WRITE);\
	sendbyte(DATA)

#endif /* INCLUDE_SSD1306_H_ */
