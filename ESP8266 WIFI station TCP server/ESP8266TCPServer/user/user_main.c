/*=======================================================================
 Name: 	 user_main.c
 Author: Roman Grintsevich
 Compile with: Eclipse Neon.2 Release (4.6.2)
 Date: 2017-03-12
 Description: Projekt -- ESP8266 station mode, TCp server.
 Purpose: school exercise 5.
 Usage: Connects to access point and acts like TCP server. Getting
 data from TCP client and shows it on the OLED with the clients IP.
=======================================================================*/

#include <ets_sys.h>
#include <osapi.h>
#include <gpio.h>
#include "driver/uart.h"
#include "user_interface.h"
#include "mem.h"

//TCP
#include <espconn.h>

//I2C
#include "driver/i2c_master.h"

//OLED
#include "driver/ssd1306.h"
#include "driver/font.h"

#define LED_GPIO 14
#define LED_GPIO2 12
#define LED_GPIO3 13
#define LED_GPIO_MUX PERIPHS_IO_MUX_MTMS_U
#define LED_GPIO_MUX2 PERIPHS_IO_MUX_MTDI_U
#define LED_GPIO_MUX3 PERIPHS_IO_MUX_MTCK_U
#define LED_GPIO_FUNC FUNC_GPIO14
#define LED_GPIO_FUNC2 FUNC_GPIO12
#define LED_GPIO_FUNC3 FUNC_GPIO13

#define DELAY 500

LOCAL os_timer_t blink_timer;
LOCAL uint8_t led_state = 0;

//WIFI
char *userSSID = "SSID"; //SSID to the connection access point
char *userPassword = "PASSWORD"; //Password of access point
struct ip_info addressInfo; //struct that will hold IP, mask and GW.
uint8_t gotIPfromAccess = 0; //Flag if received IP
//unsigned char octetLocal[4]  = {0,0,0,0};	//Array that will hold converted IP from decimal
uint8_t octetRemote[4] = {0,0,0,0}; //IP address of TCP client

//TCP
LOCAL uint16_t server_timeover = 3600; //TCP timeout 12 hours
LOCAL struct espconn servConn;
char msgToClient[100];
int msgLenght = 0;
char *msgEnd = "\r\n\0";
char clientIPstring[20]; //OLED message

//OLED
void clear_oled(void);
void printstr(char *string);
void oled_setRow(uint8_t row, uint8_t column);
void oled_printIP(unsigned char *IPtoConvert);
void oled_SendOneChar(uint8_t value);
void oled_clearRow(uint8_t row);
char oledMsg[100];

//Sensor data
const char *sensorName[4] = {"Reserved: ", "Temperature: ", "Humidity: ", "Photores: "};

LOCAL void ICACHE_FLASH_ATTR blink_cb(void *arg)
{
	//Flag is set when ESP8266 received IP
	if(gotIPfromAccess)
	{
		os_printf("IP from DHCP: %d.%d.%d.%d\n", (addressInfo.ip.addr >> (0*8)) & 0xFF, (addressInfo.ip.addr >> (1*8)) & 0xFF, (addressInfo.ip.addr >> (2*8)) & 0xFF, (addressInfo.ip.addr >> (3*8)) & 0xFF); //Print IP
		gotIPfromAccess = 0; //Disable IP printout

		/* Get IP and save in global variable octet
		uint8_t i;
		for(i = 0; i < 4; i++)
		{
			octetLocal[i] = (addressInfo.ip.addr >> (i*8)) & 0xFF; //Convert decimal to IP address
		}
		os_printf("4x. IP: %d.%d.%d.%d\n",octetLocal[0],octetLocal[1],octetLocal[2],octetLocal[3]); //Print IP.
		 */
	}
}

//Client disconnected
LOCAL void ICACHE_FLASH_ATTR espconn_tcp_disconcb(void *arg)
{
	struct espconn *pespconn = (struct espconn *) arg;

	os_printf("TCP: disconnected.\n"); //Debug
	GPIO_OUTPUT_SET(LED_GPIO2, 0); //Turn of LED indication
	clear_oled(); //Clear oled
	printstr("Client disconnected."); //Debug
}


//Get data from TCP and echo back
LOCAL void ICACHE_FLASH_ATTR espconn_tcp_recvcb(void *arg, char *pusrdata, unsigned short length)
{
	struct espconn *pespconn = (struct espconn *) arg;

	if(length > 0 && pusrdata[0] != 0x0D)
	{
		//Echo message to client containing message from server and client message
		os_strcpy(msgToClient, "From server MSG: ");

		//Add client message to the server message with LF CR and \0
		os_strcat(os_strcat(msgToClient, pusrdata), msgEnd);

		//Get lenght of the message
		msgLenght = os_strlen(msgToClient);

		//##DEBUG
		os_printf("From client #DEBUG: %s\n", pusrdata); //Print client message in debug uart

		//Get name of the sensor number sent from client
		int sensorNumber = (int)(pusrdata[0]-'0');

		//Sensor printout on the OLED and debug UART.
		if(sensorNumber < 4)
		{
			int sensorData = (int)(pusrdata[1]-'0')*10 + (int)(pusrdata[2]-'0');
			oled_clearRow(1);
			char tempString[10];
			oled_setRow(1, 0);
			printstr((char *)sensorName[sensorNumber]);
			tempString[0] = pusrdata[1];
			tempString[1] = pusrdata[2];
			tempString[2] = '\0';

			if(sensorNumber == 3)
			{
				tempString[2] = pusrdata[3];
				tempString[3] = '\0';
				sensorData = (int)(pusrdata[1]-'0')*100 + (int)(pusrdata[2]-'0')*10 + (int)(pusrdata[3]-'0');
			}

			printstr(tempString); //Print string on the OLE
			os_printf("Sensor number %d\n", sensorNumber);
			os_printf("%s%d\n", sensorName[sensorNumber],sensorData);
		}

		espconn_sent(pespconn, msgToClient, msgLenght); //Echo formated message
	}
}

//Client connected
LOCAL void ICACHE_FLASH_ATTR tcpserver_connectcb(void *arg)
{

	os_strcat(clientIPstring, "IP: ");

	struct espconn *pespconn = (struct espconn *)arg; //Connection struct

	os_printf("TCP client connected.\n");
	clear_oled(); //Clear OLED 16 chars row 8 char column

	//Get IP of TCP client
	octetRemote[0] = servConn.proto.tcp->remote_ip[0];
	octetRemote[1] = servConn.proto.tcp->remote_ip[1];
	octetRemote[2] = servConn.proto.tcp->remote_ip[2];
	octetRemote[3] = servConn.proto.tcp->remote_ip[3];

	oled_printIP(octetRemote);

	GPIO_OUTPUT_SET(LED_GPIO2, 1); //LED indication

	espconn_regist_recvcb(pespconn, espconn_tcp_recvcb);
	espconn_regist_disconcb(pespconn, espconn_tcp_disconcb);
}


//*****************

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABBBCDDD
 *                A : rf cal
 *                B : at parameters
 *                C : rf init data
 *                D : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
 *******************************************************************************/
uint32 ICACHE_FLASH_ATTR user_rf_cal_sector_set(void)
{
	enum flash_size_map size_map = system_get_flash_size_map();
	uint32 rf_cal_sec = 0;

	switch (size_map)
	{
	case FLASH_SIZE_4M_MAP_256_256:
		rf_cal_sec = 128 - 8;
		break;

	case FLASH_SIZE_8M_MAP_512_512:
		rf_cal_sec = 256 - 5;
		break;

	case FLASH_SIZE_16M_MAP_512_512:
	case FLASH_SIZE_16M_MAP_1024_1024:
		rf_cal_sec = 512 - 5;
		break;

	case FLASH_SIZE_32M_MAP_512_512:
	case FLASH_SIZE_32M_MAP_1024_1024:
		rf_cal_sec = 1024 - 5;
		break;

	default:
		rf_cal_sec = 0;
		break;
	}

	return rf_cal_sec;
}


//Wifi event handler
void eventHandler(System_Event_t *event)
{
	switch(event->event)
	{
	case EVENT_STAMODE_CONNECTED:
		os_printf("Connected as station to access point\n");
		GPIO_OUTPUT_SET(LED_GPIO, !led_state); //Turn LED on when connection established
		break;
	case EVENT_STAMODE_DISCONNECTED:
		os_printf("Disconnected from access point.\n");
		GPIO_OUTPUT_SET(LED_GPIO, led_state); //Turn of LED if disconnected
		break;
	case EVENT_STAMODE_AUTHMODE_CHANGE:
		os_printf("Event: EVENT_STAMODE_AUTHMODE_CHANGE\n");
		break;
	case EVENT_STAMODE_GOT_IP:
		os_printf("Got IP from DHCP.\n");
		wifi_get_ip_info(0, &addressInfo); //Get struct that contains IP, mask and GW.
		gotIPfromAccess = 1; //Set flag to print IP in uart.
		break;
	case EVENT_SOFTAPMODE_STACONNECTED:
		os_printf("Event: EVENT_SOFTAPMODE_STACONNECTED\n");
		break;
	case EVENT_SOFTAPMODE_STADISCONNECTED:
		os_printf("Event: EVENT_SOFTAPMODE_STADISCONNECTED\n");
		break;
	default:
		os_printf("Unexpected event: %d\n", event->event);
		break;
	}
}

//Setup function for connection to access point
void setESPstation()
{
	wifi_set_opmode_current(STATION_MODE); //Set ESP8266 as station mode
	struct station_config stationConfig; //Struct that contains information about SSID and password
	strncpy(stationConfig.ssid, userSSID, 32); //SSID
	strncpy(stationConfig.password, userPassword, 64); //Password
	os_printf("Connecting to access point...");
	wifi_station_set_config(&stationConfig); //Set SSID and password
}

//Function that will clear display
void clear_oled(void)
{
	uint8_t i;	//Amount of characters to send
	uint8_t bytesToSend;	//Amount of bytes to send

	for(i = 0; i < 128; i++) //Send 128 characters
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //8 Bytes for each character
		{
			sendbyte(font[82+bytesToSend]);
		}
	}
	STOPBIT; //Stop data communication

	//Start command communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE); //Check if slave is online
	sendbyte(COMMAND);	//Tell the slave that command will be sent
	sendbyte(SETCOL);	//Send command to set the column followed by start and end addresses bytes
	sendbyte(0); //Column start address
	sendbyte(127); //Column end address
	sendbyte(SETPAGE);	//Send command to set page, followed by start and end addresses bytes
	sendbyte(0); //Page start address
	sendbyte(7); //Page end address
	STOPBIT;

	//Start data communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE);
	sendbyte(DATA);
}

//Function to set start position, row (0-7) and column (0-15)
void oled_setRow(uint8_t row, uint8_t column)
{
	STOPBIT; //Stop data communication

	//Start command communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE); //Check if slave is online
	sendbyte(COMMAND);	//Tell the slave that command will be sent
	sendbyte(SETCOL);	//Send command to set the column followed by start and end addresses bytes
	sendbyte(column*8); //Column start address
	sendbyte(127); //Column end address
	sendbyte(SETPAGE);	//Send command to set page, followed by start and end addresses bytes
	sendbyte(row); //Page start address
	sendbyte(7); //Page end address
	STOPBIT;

	//Start data communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE);
	sendbyte(DATA);
}

//Function that sends characters to the display
//char *string - string that will be sent to the display, string must contain '\0'
void printstr(char *string)
{
	int charsToSend = 0;	//Character to send
	uint8_t bytesToSend;	//Bytes to send for each character

	while(string[charsToSend] != '\0') //Loop through string till stop bit is found
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //Each character needs 8 bytes to be displayed
		{
			sendbyte(font[string[charsToSend]*8+bytesToSend]);	//Every 8s byte is a new character in the font
		}
		charsToSend++;
	}
}

//Print IP on the OLED
void oled_printIP(unsigned char *IPtoConvert)
{
	oled_SendOneChar('I');
	oled_SendOneChar('P');
	oled_SendOneChar(':');

	oled_SendOneChar(IPtoConvert[0]/100%10 + '0');
	oled_SendOneChar(IPtoConvert[0]/10%10 + '0');
	oled_SendOneChar(IPtoConvert[0]%10 + '0');

	oled_SendOneChar('.');

	oled_SendOneChar(IPtoConvert[1]/100%10 + '0');
	oled_SendOneChar(IPtoConvert[1]/10%10 + '0');
	oled_SendOneChar(IPtoConvert[1]%10 + '0');

	oled_SendOneChar('.');

	oled_SendOneChar(IPtoConvert[2]%10 + '0');

	oled_SendOneChar('.');

	oled_SendOneChar(IPtoConvert[3]%10 + '0');
}

//Function that prints one char on the OLED
void oled_SendOneChar(uint8_t value)
{
	uint8_t bytesToSend;	//Bytes to send for each character

	for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //Each character needs 8 bytes to be displayed
	{
		sendbyte(font[value*8+bytesToSend]);
	}
}

//Clear spefic row and set cursor to the start of the row
void oled_clearRow(uint8_t row)
{
	//Start command communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE); //Check if slave is online
	sendbyte(COMMAND);	//Tell the slave that command will be sent
	sendbyte(SETCOL);	//Send command to set the column followed by start and end addresses bytes
	sendbyte(0); //Column start address
	sendbyte(127); //Column end address
	sendbyte(SETPAGE);	//Send command to set page, followed by start and end addresses bytes
	sendbyte(row); //Page start address
	sendbyte(7); //Page end address
	STOPBIT;

	//Start data communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE);
	sendbyte(DATA);

	uint8_t i;	//Amount of characters to send
	uint8_t bytesToSend;	//Amount of bytes to send

	for(i = 0; i < 16; i++) //Send 128 characters
	{
		for(bytesToSend = 0; bytesToSend < 8; bytesToSend++) //8 Bytes for each character
		{
			sendbyte(font[82+bytesToSend]);
		}
	}
	STOPBIT; //Stop data communication

	//Start command communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE); //Check if slave is online
	sendbyte(COMMAND);	//Tell the slave that command will be sent
	sendbyte(SETCOL);	//Send command to set the column followed by start and end addresses bytes
	sendbyte(0); //Column start address
	sendbyte(127); //Column end address
	sendbyte(SETPAGE);	//Send command to set page, followed by start and end addresses bytes
	sendbyte(row); //Page start address
	sendbyte(7); //Page end address
	STOPBIT;

	//Start data communication
	STARTBIT;
	sendbyte(ADDRESS+WRITE);
	sendbyte(DATA);
}

void ICACHE_FLASH_ATTR user_init(void)
{
	ets_wdt_disable();
	setESPstation(); //Set up wifi connection

	uart_init(BIT_RATE_115200, BIT_RATE_115200);

	//Wifi related event handler
	wifi_set_event_handler_cb(eventHandler);

	PIN_FUNC_SELECT(LED_GPIO_MUX, LED_GPIO_FUNC);
	PIN_FUNC_SELECT(LED_GPIO_MUX2, LED_GPIO_FUNC2);
	PIN_FUNC_SELECT(LED_GPIO_MUX3, LED_GPIO_FUNC3);

	GPIO_OUTPUT_SET(LED_GPIO, 0);
	GPIO_OUTPUT_SET(LED_GPIO2, 0);
	GPIO_OUTPUT_SET(LED_GPIO3, 0);

	os_timer_disarm(&blink_timer);
	os_timer_setfn(&blink_timer, (os_timer_func_t *) blink_cb, (void *) 0);
	os_timer_arm(&blink_timer, DELAY, 1);

	i2c_master_init(); 			//Initialize i2c
	SSD1306_INIT(HORIZONTAL); 	//Initialize Oled

	//TCP
	servConn.type = ESPCONN_TCP; //Connection type (TCP)
	servConn.state = ESPCONN_NONE;
	servConn.proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
	servConn.proto.tcp->local_port = 8888;
	espconn_regist_connectcb(&servConn, tcpserver_connectcb);
	espconn_accept(&servConn);
	espconn_regist_time(&servConn, server_timeover, 0);

	os_printf("\n");
	os_printf("Initializing TCP!\n");
}

