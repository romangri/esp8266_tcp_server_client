/*=======================================================================
 Name: 	 user_main.c
 Author: Roman Grintsevich
 Compile with: Eclipse Neon.2 Release (4.6.2)
 Date: 2017-03-12
 Description: Projekt -- ESP8266 station mode, TCP client.
 Purpose: school exercise 5.
 Usage: Connects to access point and acts like TCP server. Reads sensor
 and sends data to TCP server.
=======================================================================*/

#include <ets_sys.h>
#include <osapi.h>
#include <gpio.h>
#include "driver/uart.h"
#include "user_interface.h"
#include "mem.h"

//TCP
#include <espconn.h>

#define LED_GPIO 14
#define LED_GPIO2 12
#define LED_GPIO3 13
#define LED_GPIO_MUX PERIPHS_IO_MUX_MTMS_U
#define LED_GPIO_MUX2 PERIPHS_IO_MUX_MTDI_U
#define LED_GPIO_MUX3 PERIPHS_IO_MUX_MTCK_U
#define LED_GPIO_FUNC FUNC_GPIO14
#define LED_GPIO_FUNC2 FUNC_GPIO12
#define LED_GPIO_FUNC3 FUNC_GPIO13

#define DELAY 500

LOCAL os_timer_t blink_timer;
LOCAL uint8_t led_state = 0;

//WIFI
char *userSSID = "SSID"; //SSID to the connection access point
char *userPassword = "PASSWORD"; //Password of access point
struct ip_info addressInfo; //struct that will hold IP, mask and GW.
uint8_t gotIPfromAccess = 0; //Flag if received IP

//TCP
struct espconn conn1;
esp_tcp tcp1;
uint8 ipAddress[4] = {192,168,1,2}; //Server IP
uint8_t connectedToServer = 0; //Connection flag to TCP server

//Sensor
os_timer_t sensorTimer; //Timer to read ADC
uint32_t timeToSend = 0; //Flag on how often ADC should read the value

LOCAL void ICACHE_FLASH_ATTR blink_cb(void *arg)
{
	//Flag is set when ESP8266 received IP
	if(gotIPfromAccess)
	{
		os_printf("IP from DHCP: %d.%d.%d.%d\n", (addressInfo.ip.addr >> (0*8)) & 0xFF, (addressInfo.ip.addr >> (1*8)) & 0xFF, (addressInfo.ip.addr >> (2*8)) & 0xFF, (addressInfo.ip.addr >> (3*8)) & 0xFF); //Print IP
		gotIPfromAccess = 0; //Disable IP printout

		/* Get IP and save in global variable octet
			uint8_t i;
			for(i = 0; i < 4; i++)
			{
				octetLocal[i] = (addressInfo.ip.addr >> (i*8)) & 0xFF; //Convert decimal to IP address
			}
			os_printf("4x. IP: %d.%d.%d.%d\n",octetLocal[0],octetLocal[1],octetLocal[2],octetLocal[3]); //Print IP.
		 */
	}
}

//Get data from TCP and echo back
LOCAL void ICACHE_FLASH_ATTR espconn_tcp_recvcb(void *arg, char *pusrdata, unsigned short length)
{
	struct espconn *pespconn = (struct espconn *) arg;

	if(length > 0)
	{
		//Debug
		os_printf("%s", pusrdata);
	}
}

//Sent event on the TCP
LOCAL void ICACHE_FLASH_ATTR espconn_tcp_sentcb(void *arg)
{
	os_printf("Data sent to TCP server.\n"); //Debug
	espconn_disconnect(&conn1); //Disconnect from TCP server
}

//TCP disconnet event
LOCAL void ICACHE_FLASH_ATTR disconnCB(void *arg)
{
	os_printf("Disconnected from TCP server.\n"); //Debug
	connectedToServer = 0; //Reset flag for TCP sending when disconnected
}

//TCP connect event
LOCAL void ICACHE_FLASH_ATTR connectCB(void *arg)
{
	struct espconn *pespconn = (struct espconn *) arg;

	os_printf("Connected to TCP server.\n"); //Debug
	espconn_regist_recvcb(pespconn, espconn_tcp_recvcb); //Register receive on TCP
	espconn_regist_sentcb(pespconn, espconn_tcp_sentcb); //Register transmit on TCP
	espconn_regist_disconcb(pespconn, disconnCB); //Register disconnect from TCP server

	connectedToServer = 1; //Set flag to read ADC and send to TCP server
}

//Timer callback
//Voltage on the ADC pin 0V - 0.850V
void timerCallback(void *arg)
{
	timeToSend++;

	//Send ADC reading to the TCP server each 10 seconds
	if(timeToSend == 3)
	{
		espconn_connect(&conn1); //Connect to TCP server
		os_delay_us(1000); //Delay before TCP connection is established

		uint16 sensorValue = 0;	//Variable for sensor
		sensorValue = system_adc_read(); //Read ADC
		os_printf("ADC read: %d\n", sensorValue); //Debug

		if(connectedToServer)
		{
			char valueToSend[4] = {3+'0',sensorValue/100%10+'0',sensorValue/10%10+'0',sensorValue%10+'0',}; //Convert from int values to char
			espconn_send(&conn1, valueToSend, 4); //Send ADC reading to TCP server
		}

		timeToSend = 0;
	}
}

/******************************************************************************
 * FunctionName : user_rf_cal_sector_set
 * Description  : SDK just reversed 4 sectors, used for rf init data and paramters.
 *                We add this function to force users to set rf cal sector, since
 *                we don't know which sector is free in user's application.
 *                sector map for last several sectors : ABBBCDDD
 *                A : rf cal
 *                B : at parameters
 *                C : rf init data
 *                D : sdk parameters
 * Parameters   : none
 * Returns      : rf cal sector
 *******************************************************************************/
uint32 ICACHE_FLASH_ATTR user_rf_cal_sector_set(void)
{
	enum flash_size_map size_map = system_get_flash_size_map();
	uint32 rf_cal_sec = 0;

	switch (size_map)
	{
	case FLASH_SIZE_4M_MAP_256_256:
		rf_cal_sec = 128 - 8;
		break;

	case FLASH_SIZE_8M_MAP_512_512:
		rf_cal_sec = 256 - 5;
		break;

	case FLASH_SIZE_16M_MAP_512_512:
	case FLASH_SIZE_16M_MAP_1024_1024:
		rf_cal_sec = 512 - 5;
		break;

	case FLASH_SIZE_32M_MAP_512_512:
	case FLASH_SIZE_32M_MAP_1024_1024:
		rf_cal_sec = 1024 - 5;
		break;

	default:
		rf_cal_sec = 0;
		break;
	}

	return rf_cal_sec;
}

//Wifi event handler
void eventHandler(System_Event_t *event)
{
	switch(event->event)
	{
	case EVENT_STAMODE_CONNECTED:
		os_printf("Connected as station to access point\n");
		GPIO_OUTPUT_SET(LED_GPIO, !led_state); //Turn LED on when connection established
		break;
	case EVENT_STAMODE_DISCONNECTED:
		os_printf("Disconnected from access point.\n");
		GPIO_OUTPUT_SET(LED_GPIO, led_state); //Turn of LED if disconnected
		break;
	case EVENT_STAMODE_AUTHMODE_CHANGE:
		os_printf("Event: EVENT_STAMODE_AUTHMODE_CHANGE\n");
		break;
	case EVENT_STAMODE_GOT_IP:
		os_printf("Got IP from DHCP.\n");
		wifi_get_ip_info(0, &addressInfo); //Get struct that contains IP, mask and GW.
		gotIPfromAccess = 1; //Set flag to print IP in uart.
		break;
	case EVENT_SOFTAPMODE_STACONNECTED:
		os_printf("Event: EVENT_SOFTAPMODE_STACONNECTED\n");
		break;
	case EVENT_SOFTAPMODE_STADISCONNECTED:
		os_printf("Event: EVENT_SOFTAPMODE_STADISCONNECTED\n");
		break;
	default:
		os_printf("Unexpected event: %d\n", event->event);
		break;
	}
}

//Setup function for connection to access point
void setESPstation()
{
	wifi_set_opmode_current(STATION_MODE); //Set ESP8266 as station mode
	struct station_config stationConfig; //Struct that contains information about SSID and password
	strncpy(stationConfig.ssid, userSSID, 32); //SSID
	strncpy(stationConfig.password, userPassword, 64); //Password
	os_printf("Connecting to access point...");
	wifi_station_set_config(&stationConfig); //Set SSID and password
}

void ICACHE_FLASH_ATTR user_init(void)
{
	ets_wdt_disable();
	setESPstation(); //Set up wifi connection

	uart_init(BIT_RATE_115200, BIT_RATE_115200);

	//Wifi related event handler
	wifi_set_event_handler_cb(eventHandler);

	PIN_FUNC_SELECT(LED_GPIO_MUX, LED_GPIO_FUNC);
	PIN_FUNC_SELECT(LED_GPIO_MUX2, LED_GPIO_FUNC2);
	PIN_FUNC_SELECT(LED_GPIO_MUX3, LED_GPIO_FUNC3);
	GPIO_OUTPUT_SET(LED_GPIO, 1);
	GPIO_OUTPUT_SET(LED_GPIO2, 1);
	GPIO_OUTPUT_SET(LED_GPIO3, 0);
	os_timer_disarm(&blink_timer);
	os_timer_setfn(&blink_timer, (os_timer_func_t *) blink_cb, (void *) 0);
	os_timer_arm(&blink_timer, DELAY, 1);


	//TCP
	tcp1.remote_port = 8888;	//Server port
	tcp1.remote_ip[0] = ipAddress[0]; //Server IP
	tcp1.remote_ip[1] = ipAddress[1];
	tcp1.remote_ip[2] = ipAddress[2];
	tcp1.remote_ip[3] = ipAddress[3];
	tcp1.local_port = espconn_port(); //Outgoing TCP port
	struct ip_info ipconfig;
	wifi_get_ip_info(STATION_IF, &ipconfig);
	os_memcpy(tcp1.local_ip, &ipconfig.ip, 4);
	conn1.type = ESPCONN_TCP; //Connection type set to TCP
	conn1.state = ESPCONN_NONE;
	conn1.proto.tcp = &tcp1;
	espconn_regist_connectcb(&conn1, connectCB); //Register connection event for TCP

	os_timer_setfn(&sensorTimer, timerCallback, NULL); //Set timer for the ADC read
	os_timer_arm(&sensorTimer, 3000, 1); //Every 3 seconds, call function with ADC read and sending data to the TCP server
}
